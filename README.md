# AWS OCP4.2 Install

## Prerequisite for OCP

### DNS in AWS

- Preapre DNS(Route53) : darumtech.net

![dns](img/2019-12-02-13-38-57.png) 
- Follow guide line : https://docs.openshift.com/container-platform/4.2/installing/installing_aws/installing-aws-account.html

### Prepare for access and secret key

- goto IAM in aws web console
- create user 
![](img/2019-12-02-13-52-36.png) 
![](img/2019-12-02-13-53-02.png) 
- set permission 
![](img/2019-12-02-13-53-34.png) 
- set tag(optional) 
![](img/2019-12-02-13-54-03.png) 
![](img/2019-12-02-13-54-35.png) 
- save access key and secet key 
![](img/2019-12-02-13-55-15.png)

### keygen

``` shell
ssh-keygen -t rsa -b 4096 -N '' -f ./key/aws_rsa -C ibm
ssh-add ./key/aws_rsa
```

### register pem into AWS as keypair

![aws](img/2019-12-02-13-35-58.png)

![key](img/2019-12-02-13-37-12.png)

### prepare the installation dir

``` shell
openshift-install create install-config --dir=./install
```
- select ssh public key 
![](img/2019-12-02-13-49-06.png) 
- select provider as aws 
![](img/2019-12-02-13-50-07.png)
- input Access key 
![](img/2019-12-02-13-57-01.png) 
- input secret key 
![](img/2019-12-02-13-57-29.png) 
- select aws region to apnorth2 using arrow down 
![](img/2019-12-02-13-58-17.png) 
- select dns registered in aws route53 
![](img/2019-12-02-13-58-55.png) 
- input cluster name : msg 
![](img/2019-12-02-14-00-49.png) 
- copy pull sceret from redhat(https://cloud.redhat.com/openshift/install/aws/installer-provisioned) 
![](img/2019-12-02-14-02-19.png) 
![](img/2019-12-02-14-02-56.png) 
- check install-config.yaml generated in ./install
![](img/2019-12-02-14-08-58.png)

## install ocp4.2

``` shell
openshift-install create cluster --dir=./install --log-level=debug | tee install.log
```


## Result

### bootstrap

![](img/2019-12-02-14-20-14.png)

``` shell


INFO Waiting up to 10m0s for the openshift-console route to be created... 
DEBUG Route found in openshift-console namespace: console 
DEBUG Route found in openshift-console namespace: downloads 
DEBUG OpenShift console route is created           
INFO Install complete!                            
INFO To access the cluster as the system:admin user when using 'oc', run 'export KUBECONFIG=/Users/seungil/Workspace/cp4x/ocp42-aws/install/auth/kubeconfig' 
INFO Access the OpenShift web-console here: https://console-openshift-console.apps.msg.darumtech.net 
INFO Login to the console with user: kubeadmin, password: VGb2s-G95th-QM3iR-4aNs5 


```

### create user and passwd using htpasswd

``` shell
htpasswd -c -B -b users admin [password]
htpasswd -b users dev [password]
```